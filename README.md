# spbspu-labs-2020-cmake
![alt text](img/clion-student-showcase.png "Пример в CLion")
![alt text](img/clion-showcase.png "Пример в CLion (без .student)")
## Инструкция
1. Скопировать `CMakeLists.txt` в корневую папку репозитория.
2. Создать файл `.student`, в котором должно быть название вашей папки, например:
`kuzmas.vinya`
3. Использовать.
    - В CLion необходимо правой кнопкой мыши `Load CMake Project` по `CMakeLists.txt`
    
        ![alt text](img/clion-load-cmake-project.png "Load CMake Project")
## Инструкция 2
- При **добавлении** или **удалении файла** (исходника, заголовочного) необходим reload.

    ![alt text](img/clion-reload-cmake-project.png "Reload CMake Project")
- Поддерживается `.boost_location`.
Туда можно вписать местоположение библиотеки Boost.
- При добавлении файлов в CLion уберите галОчку с `Add to targets`.
- Не рекомендуется использовать MSVC. Лучше всего будет WSL + (GCC или Clang). 
Или, на худой конец, Cygwin или MinGW.
